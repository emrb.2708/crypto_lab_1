module Simple
def mod_emp(base, exponent, modulus)
  result = 1
  while exponent > 0
    if exponent % 2 == 1
      result = (result * base) % modulus
    end
    base = (base * base) % modulus
    exponent = exponent / 2
  end
  result
end

def solovay_strassen(n, k)
  return false if n < 2
  return true if n < 4

  a = 0 
  x = 0
  k.times do
      a = 2 + rand(n - 3)
      x = mod_emp(a, (n - 1) / 2, n)

      return false if x != 1 && x != n - 1
    end

    jacobi = jacobi_s(a, n)
    return false if jacobi != x % n
  return true
end

def jacobi_s(a, n)
  if a == 0
    return n == 1 ? 1 : 0
  elsif a % 2 == 0
    return jacobi_s(a / 2, n) * ((n * n - 1) % 8 + 1)
  elsif a >= n
    return jacobi_s(a % n, n)
  else
    return (-1) ** ((a - 1) * (n - 1) / 4) * jacobi_s(n, a)
  end
end
end